from __future__ import unicode_literals
from django.urls.conf import path, include
from rest_framework import routers
from .views import HomeView
from .views import TaskViewSet, TaskListViewSet


router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)
router.register(r'tasklists', TaskListViewSet)


urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('', HomeView.as_view(), name='home')
]
