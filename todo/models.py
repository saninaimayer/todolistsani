from django.db import models
from django.utils.translation import ugettext_lazy as _


class Task(models.Model):
    description = models.CharField(max_length=255, verbose_name=_('Descrição'))
    task_date = models.DateField(verbose_name=_('Data'))
    is_completed = models.BooleanField(verbose_name=_('Tarefa concluída?'), default=False)

    class Meta:
        ordering = ('task_date', 'is_completed')
        verbose_name = _('Tarefa')
        verbose_name_plural = _('Tarefas')


    def __str__(self):
        return '{} - {}'.format(self.task_date, self.description)

    def task_list(self):
        '''
            Returns which list a task belongs to 
        '''
        return self.tasklists.first()

    def status(self):
        '''
            Return task status as String
        '''
        return 'Concluída' if self.is_completed else 'Não concluída'


class TaskList(models.Model):
    name = models.CharField(max_length=100, db_index=True, verbose_name=_('Nome'))
    tasks = models.ManyToManyField(Task, verbose_name=_('Tarefas'), related_name='tasklists', blank=True)

    class Meta:
        ordering = ('pk', )
        verbose_name = _('Lista de tarefa')
        verbose_name_plural = _('Lista de tarefas')


    def __str__(self):
        return self.name
