$(document).ready(function() {
    $('#formTaskListModel').on('hide.bs.modal', function (event) {
        var form = $('#formTaskListModel form');
        form.attr('data-tasklist', '');
        form.prop('method', 'POST');
        form.find('input[name=name]').val('');
        form.find('select[name=tasks]').val('');
        $('#deleteTaskListButton').prop('disabled', true);
    });

    $('a.tasklist').click(function(e) {
        var taskListId = $(this).attr('data-tasklist');

        $.ajax({
            url: '/api/v1/tasklists/' + taskListId,
            type: "GET",
            dataType: "json",
            success: function(data) {
                var form = $('#formTaskListModel form');
                form.attr('data-tasklist', data.id);
                form.prop('method', 'PUT');
                form.find('input[name=name]').val(data.name);
                form.find('select[name=tasks]').val(data.tasks);
                $('#deleteTaskListButton').prop('disabled', false);
                $("#addTaskListButton").click();
            },
            error: function(xhr, errMsg) {
                alert('Não foi pssível carregar a lista.');
            }
        });
    });

    $('#saveTaskListButton').click(function(e) {
        var form = $('#formTaskListModel form');
        var data = form.serialize();
        var taskListId = form.attr('data-tasklist');
        var taskListMethod = form.attr('method');
        var taskListUrl = '/api/v1/tasklists/';

        if (!isNaN(taskListId)) {
            taskListUrl = '/api/v1/tasklists/' + taskListId + '/';
        }

        $.ajax({
            url: taskListUrl,
            type: taskListMethod,
            dataType: "json",
            data: data,
            success: function(data) {
                alert('Lista salva com sucesso.');
                document.location = document.location;
            },
            error: function(xhr, errMsg) {
                alert('Não foi pssível salvar a lista.');
            }
        });
    });

    $('#deleteTaskListButton').click(function(e) {
        var form = $('#formTaskListModel form');
        var taskListId = form.attr('data-tasklist');
        if (!isNaN(taskListId)) {
            if (confirm("Deseja realmente excluir esta lista de tarefas?")) {
                $.ajax({
                    url: '/api/v1/tasklists/' + taskListId + '/',
                    type: 'DELETE',
                    dataType: "json",
                    success: function(data) {
                        alert('Lista removida com sucesso');
                        document.location = document.location;
                    },
                    error: function(xhr, errMsg) {
                        alert('Não foi pssível remover a lista.');
                    }
                });
            }
        }
    });
});
