$(document).ready(function() {
    $('#formTaskModel').on('hide.bs.modal', function (event) {
        var form = $('#formTaskModel form');
        form.attr('data-task', '');
        form.prop('method', 'POST');
        form.find('input[name=description]').val('');
        form.find('input[name=task_date]').val('');
        form.find('input[name=is_completed]').prop('checked', false);
        $('#deleteTaskButton').prop('disabled', true);
    });

    $('a.task').click(function(e) {
        var taskId = $(this).attr('data-task');

        $.ajax({
            url: '/api/v1/tasks/' + taskId,
            type: "GET",
            dataType: "json",
            success: function(data) {
                var form = $('#formTaskModel form');
                form.attr('data-task', data.id);
                form.prop('method', 'PUT');
                form.find('input[name=description]').val(data.description);
                form.find('input[name=task_date]').val(data.task_date);
                form.find('input[name=is_completed]').prop('checked', data.is_completed);
                $('#deleteTaskButton').prop('disabled', false);
                $("#addTaskButton").click();
            },
            error: function(xhr, errMsg) {
                alert('Não foi pssível carregar a tarefa.');
            }
        });
    });

    $('#saveTaskButton').click(function(e) {
        var form = $('#formTaskModel form');
        var data = form.serialize();
        var taskId = form.attr('data-task');
        var taskMethod = form.attr('method');
        var taskUrl = '/api/v1/tasks/';

        if (!isNaN(taskId)) {
            taskUrl = '/api/v1/tasks/' + taskId + '/';
        }

        $.ajax({
            url: taskUrl,
            type: taskMethod,
            dataType: "json",
            data: data,
            success: function(data) {
                alert('Tarefa salva com sucesso.');
                document.location = document.location;
            },
            error: function(xhr, errMsg) {
                alert('Não foi pssível salvar a tarefa.');
            }
        });
    });

    $('#deleteTaskButton').click(function(e) {
        var form = $('#formTaskModel form');
        var taskId = form.attr('data-task');
        if (!isNaN(taskId)) {
            if (confirm("Deseja realmente excluir esta tarefa?")) {
                $.ajax({
                    url: '/api/v1/tasks/' + taskId + '/',
                    type: 'DELETE',
                    dataType: "json",
                    success: function(data) {
                        alert('Tarefa removida com sucesso');
                        document.location = document.location;
                    },
                    error: function(xhr, errMsg) {
                        alert('Não foi pssível remover a tarefa.');
                    }
                });
            }
        }
    });
});