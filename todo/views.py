from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from django.shortcuts import render
from rest_framework import viewsets
from .models import Task, TaskList
from .serializers import TaskSerializer, TaskListSerializer
from rest_framework.request import Request
from rest_framework.views import APIView


class HomeView(View):
    def get(self, request):
        task_serializer = TaskSerializer()
        tasklist_serializer_context = {
            'request': Request(request),
        }
        tasklist_serializer = TaskListSerializer(context=tasklist_serializer_context)
        task_list = TaskList.objects.all()
        single_tasks = Task.objects.filter(tasklists__isnull=True)
        return render(request, 'public/home.html', {'task_list': task_list, 'single_tasks': single_tasks,
                                                    'task_serializer': task_serializer, 
                                                    'tasklist_serializer': tasklist_serializer})


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskListViewSet(viewsets.ModelViewSet):
    queryset = TaskList.objects.all()
    serializer_class = TaskListSerializer
