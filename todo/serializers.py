from __future__ import unicode_literals
from rest_framework import serializers
from .models import Task, TaskList
from django.db.models import Q
from rest_framework.fields import empty
from django.utils.translation import ugettext_lazy as _


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'description', 'task_date', 'is_completed']


class TaskListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TaskList
        fields = ['id', 'name', 'tasks']


    def save(self, **kwargs):
        '''
            Override method due to the proposed modeling for the exercise
        '''
        validated_data = dict(self.validated_data)
        tasks = validated_data['tasks']

        for task in tasks:
            task_list = task.task_list()
            if task_list:
                task_list.tasks.remove(task)

        return super().save(**kwargs)
